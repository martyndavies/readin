import pytest
from util import make_bool


@pytest.mark.parametrize("test_input,expected", [
    ("yes",True),
    ("y", True),
    ("YES", True),
    ("TRUE", True),
    ("true", True),
    ("T", True),
    ("no",False),
    ("n", False),
    ("NO", False),
    ("FALSE", False),
    ("False", False),
    ("F", False),
    ("banjax", False)
])
def test_bool_convert(test_input, expected):
    result = make_bool(test_input)
    assert result == expected