import json
import os


class JsonInput:

    def __init__(self,path):
        self.path = path

    def readin(self):
        if not os.path.exists(self.path):
            raise Exception(FileNotFoundError)

        self.fd = open(self.path,'r')
        data = self.fd.read()
        obj = json.loads(data)
        return obj

    def __delete__(self):
        self.fd.close()