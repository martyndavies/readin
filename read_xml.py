import xmltodict
import os


class XMLInput:

    def __init__(self,path,do_unzip=True):
        self.path = path
        self.do_unzip = do_unzip

    def readin(self):
        if not os.path.exists(self.path):
            raise Exception(FileNotFoundError)

        if self.do_unzip:
            self.unzip()

        obj = None
        with open(self.path,'r') as fd:
            obj = xmltodict.parse(fd.read())
        return obj

    def __delete__(self):
        self.fd.close()

    def unzip(self):
        pass


