
class WriteCSV:

    def __init__(self,path):
        self.path = path
        self.outfd = open(path,'w')

    def write_csv_header(self):
        pass

    def write_item(self,id,name,brand,retailer,price,in_stock,source):
        line = """ "%s", "%s", "%s", "%s", %f, "%s", "%s" """ % (id,name,brand,retailer,price,in_stock,source)
        self.outfd.write(line)

    def __del__(self):
        self.outfd.close()

