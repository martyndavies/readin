import pytest
from unittest.mock import MagicMock
from read_xml import XMLInput

def test_read_obj(monkeypatch):

    mock_file = MagicMock()
    mock_file.read = MagicMock()
    mock_file.read.return_value = '/// not XML data ///'

    mock_exists = MagicMock(return_value=True)
    monkeypatch.setattr('os.path.exists', mock_exists)

    mock_open = MagicMock(return_value=mock_file)
    monkeypatch.setattr('builtins.open', mock_open)

    fnm = '/tmp/readr.xml'
    xmlInput = XMLInput(fnm)

    with pytest.raises(TypeError):
        result = xmlInput.readin()

    mock_open.assert_called_once_with(fnm,'r')


def test_readXML(monkeypatch):
    xml = "<note></note>"
    mock_file = MagicMock()
    mock_file.read = MagicMock()
    mock_file.read.return_value = xml

    mock_exists = MagicMock(return_value=True)
    monkeypatch.setattr('os.path.exists', mock_exists)

    mock_open = MagicMock(return_value=mock_file)
    monkeypatch.setattr('builtins.open', mock_open)

    mockTranslate = MagicMock(return_value=xml)
    monkeypatch.setattr('xmltodict.parse', mockTranslate)

    fnm = '/tmp/readr.xml'
    xmlInput = XMLInput(fnm)
    result = xmlInput.readin()
    print("RESULT %s" % result)

    mock_open.assert_called_once_with(fnm,'r')
    assert result == xml