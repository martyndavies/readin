import pytest
from unittest.mock import MagicMock
import json

from read_json import JsonInput


def test_readJson(monkeypatch):
    obj = { 'this-is':'data'}
    some_json = json.dumps(obj)

    mock_file = MagicMock()
    mock_file.read = MagicMock()
    mock_file.read.return_value = some_json

    mock_exists = MagicMock(return_value=True)
    monkeypatch.setattr('os.path.exists', mock_exists)

    mock_open = MagicMock(return_value=mock_file)
    monkeypatch.setattr('builtins.open', mock_open)

    jsonInput = JsonInput('x.json')
    result = jsonInput.readin()

    mock_open.assert_called_once_with('x.json','r')

    assert result == obj


def test_readJson_bad_contents(monkeypatch):

    mock_file = MagicMock()
    mock_file.read = MagicMock()
    mock_file.read.return_value = '/// not JSON data ///'

    mock_exists = MagicMock(return_value=True)
    monkeypatch.setattr('os.path.exists', mock_exists)

    mock_open = MagicMock(return_value=mock_file)
    monkeypatch.setattr('builtins.open', mock_open)

    jsonInput = JsonInput('x.json')
    with pytest.raises(ValueError):
        result = jsonInput.readin()

    mock_open.assert_called_once_with('x.json','r')


def test_file_not_exist(monkeypatch):
    mock_exists = MagicMock(return_value=False)
    monkeypatch.setattr('os.path.exists', mock_exists)
    jsonInput = JsonInput('x.json')
    with pytest.raises(Exception):
        result = jsonInput.readin()





