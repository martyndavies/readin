
def make_bool(param):
    if isinstance(param,bool):
        return param
    elif isinstance(param,str):
        word = param.lower()
        if word[0] == 't' or word[0] == 'y':
            return True
        elif word[0] == 'f' or word[0] == 'n':
            return False
    return False


def parsed_item(item):
    try:
        id = item['id']
    except KeyError:
        return None

    try:
        name = item['name']
    except KeyError:
        name = '{unknown}'

    try:
        brand = item['brand']
    except:
        brand = '{unknown}'

    try:
        retailer = item['retailer']
    except:
        retailer = '{unknown}'

    try:
        price = float( item['price'])
    except:
        price = 0.0

    try:
        in_stock = make_bool( item['in_stock'] )
    except:
        in_stock = False

    return id,name,brand,retailer,price,in_stock