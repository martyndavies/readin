import pytest
from unittest.mock import MagicMock
import json

from writeout import WriteCSV


def test_open(monkeypatch):
    sample_data = ' "aaa", "Shreddies", "Nabisco", "Tesco", 2.150000, "True", "CSV" '

    mock_file = MagicMock()
    mock_file.write = MagicMock()
    mock_file.write.return_value = len(sample_data)

    mock_open = MagicMock(return_value=mock_file)
    monkeypatch.setattr('builtins.open', mock_open)

    fake_file_path = '/tmp/y.csv'
    writeCSV = WriteCSV(fake_file_path)
    writeCSV.write_item('aaa','Shreddies','Nabisco','Tesco',2.15,True,'CSV')

    mock_open.assert_called_once_with(fake_file_path,'w')
    mock_file.write.assert_called_with(sample_data)


