
from read_json import JsonInput
from util import make_bool, parsed_item
from writeout import WriteCSV

# open output file ready for input processing, and write column headers
csvOutput = WriteCSV('/tmp/output.csv')
csvOutput.write_header('id, name, brand, retailer, price, in_stock')

# first read in JSON file
jsonInput = JsonInput('/tmp/products.json')
rows = jsonInput.readin()
source = 'JSON'
for item in rows:
    id, name, brand, retailer, price, in_stock = parsed_item(item)
    csvOutput.write_item(id, name, brand, retailer, price, in_stock, source)


# Now gunzip ZIP file into CSV and read CSV file also; write to output
source = 'CSV'

# Finally unxzip and parse XML file and write to CSV
source = 'XML'


